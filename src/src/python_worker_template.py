# cython: language_level=3
from MCloud import *
from S2STime import *
from WorkerUserData import *
import subprocess
from MCloudPacketRCV import MCloudPacketRCV
import os
import numpy as np
import time
import threading
from  decode_asr import *
import argparse
import re
import sys

TEMP_DIR = "/tmp/recognizer"
cache_text = ""
from segmentor_asr import Segmentor

def segment(mcloud):


    err = 0
    # connect to mediator
    res_connect = mcloud.connect(serverHost.encode("utf-8"), serverPort)
    i = 0
    if res_connect == 1:
        print("ERROR Connection established")
    else:
        print("WORKER INFO Connection established ==> waiting for clients.")

    while res_connect == 0:
        # wait for client
        res = mcloud.wait_for_client(stream_id.encode("utf-8"))

        proceed = False
        if res == 1:
            print("WORKER ERROR while waiting for client")
            break
        elif res == 0:

            proceed = True
            print("WORKER INFO received client request ==> waiting for packages")
        while (proceed):

            packet = MCloudPacketRCV(mcloud)

            type = packet.packet_type()
            if packet.packet_type() == 3:

                mcloud.process_data_async(packet, data_callback)

            elif packet.packet_type() == 7:  # MCloudFlush
                """
                a flush message has been received -> wait (block) until all pending packages
                from the processing queue has been processed -> finalizeCallback will
                be called-> flush message will be passed to subsequent components
                """
                mcloud.wait_for_finish(0, "processing")
                mcloud.send_flush()
                print("WORKER INFO received flush message ==> waiting for packages.")
                mcloudpacketdenit(packet)
                break
            elif packet.packet_type() == 4:  # MCloudDone

                print("WOKRER INFO received DONE message ==> waiting for clients.")
                mcloud.wait_for_finish(1, "processing")
                mcloudpacketdenit(packet)
                clean(tempDir)
                proceed = False
            elif packet.packet_type() == 5:  # MCloudError
                # In case of a error or reset message, the processing is stopped immediately by
                # calling mcloudBreak followed by exiting the thread.
                mcloud.wait_for_finish(1, "processing")
                mcloud.stop_processing("processing")
                mcloudpacketdenit(packet)
                clean(tempDir)
                print("WORKER INFO received ERROR message >>> waiting for clients.")
                proceed = False
            elif packet.packet_type() == 6:  # MCloudReset
                mcloud.stop_processing("processing")

                print("CLIENT INFO received RESET message >>> waiting for clients.")
                mcloudpacketdenit(packet)
                clean(tempDir)
                proceed = False
            else:
                print("CLIENT ERROR unknown packet type {!s}".format(packet.packet_type()))
                proceed = False
                err = 1
            if err == 1:
                break
        print("WORKER WARN connection terminated ==> trying to reconnect.")

def printusage():
    print("\n")
    print("\nNAME\n\t%s - Speech Recognition Backend.\n");
    print("\nSYNOPSIS\n\t%s [OPTION]... ASRCONFIGFILE\n");
    print("\nDESCRIPTION\n""\tThis is a example implementation of an ASR backend which connects to""\tMTEC's Mediator in the cloud.\n");
    print("\nOPTION\n""\t-s, --server=HOSTNAME\n\t\tHost name of the server where the Mediator is running.\n\n""\t-p, --serverPort=PORT\n\t\tPort address at which the Mediator accepts worker.\n\n""\t-h, --help\n\t\tShows this help.\n\n")
    return 0

def processing_finalize_callback():
    segmentor_asr.reset()
    print("INFO in processing finalize callback")



def processing_error_callback():
    segmentor_asr.reset()
    print("INFO In processing error callback")




def processing_break_callback():
    segmentor_asr.reset()
    print("INFO in processing break callback")



def init_callback():

    clean(tempDir)

    print("INFO in processing init callback ")

def data_callback(i,sampleA):
    sample = np.asarray(sampleA,dtype=np.int16)

    segmentor_asr.add_audio(sample.tobytes())
    return  0

def clean(tempDir):
    if os.path.exists(tempDir):
        subprocess.call(["rm -rf %s/*.fgets" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*.idx" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*ready" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*start" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*info" % tempDir], shell=True)


parser = argparse.ArgumentParser(description='pynn')
#model argument

#worker argument
parser.add_argument('-s','--server', type=str, default="i13srv53.ira.uka.de")
parser.add_argument('-p','--port' ,type=int, default=60019)
parser.add_argument('-fi','--fingerprint', type=str, default="en-EU")
parser.add_argument('-fo','--outfingerprint',type=str, default="en-EU")
parser.add_argument('-i','--inputType' ,type=str, default="audio")
parser.add_argument('-o','--outputType', type=str, default="unseg-text")
#segmentor argument
sample_rate = 16000
VAD_aggressive = 2
padding_duration_ms = 450
frame_duration_ms = 30
rate_begin = 0.65
rate_end = 0.55

args = parser.parse_args()
serverHost = args.server
serverPort = args.port
inputFingerPrint  = args.fingerprint
inputType         = args.inputType
outputFingerPrint = args.outfingerprint
outputType        = args.outputType
specifier           = ""
stream_id = ""
tempDir = TEMP_DIR
print("Initialize the Transformer model...")

segmentor_asr = Segmentor(tempDir, sample_rate, VAD_aggressive, padding_duration_ms, frame_duration_ms, rate_begin, rate_end)
#init_asr_model
model = None


print("Done.")
print("Waiting for audio to decode...")


print("#" * 40 + " >> TESTING MCLOUD WRAPPER API << " + "#" * 40)
mcloud_w = MCloudWrap("asr".encode("utf-8"), 1)
mcloud_w.add_service("MTEC asr".encode("utf-8"), "asr".encode("utf-8"), inputFingerPrint.encode("utf-8"), inputType.encode("utf-8"),outputFingerPrint.encode("utf-8"), outputType.encode("utf-8"), specifier.encode("utf-8"))

#set callback
mcloud_w.set_callback("init", init_callback)
mcloud_w.set_data_callback("worker")
mcloud_w.set_callback("finalize", processing_finalize_callback)
mcloud_w.set_callback("error", processing_error_callback)
mcloud_w.set_callback("break", processing_break_callback)

#clean tempfile
if os.path.exists(tempDir):
    subprocess.call(["rm -rf %s/*" % tempDir], shell=True)
else:
    os.mkdir(tempDir)




#segmentor thread
record = threading.Thread(target=segment, args=(mcloud_w,))
record.start()

cache_text = ""
tempDir = TEMP_DIR
recList = "%s/recording.adc.en-EN.fgets" % tempDir
#resending = True
try:
    while True:
        if not os.path.exists(recList):
            time.sleep(0.2)
            continue
        fin = open(recList, 'r+')
        while True:
            pos = fin.tell()
            line = fin.readline()
            if line is None or line == '': break
            if line.startswith('#'): continue
            adc = line[:-1].split()[0]
            line = '#' + line[1:]
            fin.seek(pos, 0)
            fin.write(line)
            adc_ready = "%s-ready" % adc
            adc_info = "%s-info" % adc
            if not os.path.exists(adc_info):
                time.sleep(0.1)
                continue
            
            seg_info = open(adc_info, "r").read()
            start_time = float(re.findall("startS \d+", seg_info)[0][7::])/16
            
            n_time = 1000

            while not os.path.exists(adc_ready):
                sec = os.path.getsize(adc) // (16*2)
                

                if sec > ntime:          
                    ntime += 1000     
                    hypo = decode(model, adc)
                    end_time = start_time + sec 
                    mcloud_w.send_packet_result_async(start_time, end_time, hypo, len(hypo))                            

                time.sleep(0.1)
            # decode the audio segment
            hypo,  = decode(model, adc)
            sec = os.path.getsize(adc) // (16*2)
            end_time = start_time + sec 
            mcloud_w.send_packet_result_async(start_time, end_time, hypo, len(hypo))
            print('Finished ADC: %s' % adc)
        fin.close()
        time.sleep(0.2)
except KeyboardInterrupt:
    print("Terminating running threads..")

record.join()
