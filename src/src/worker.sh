#!/bin/bash

SYSTEM_PATH=`dirname "$0"`

export LD_LIBRARY_PATH=../linux_lib64
export PYTHONPATH=$SYSTEM_PATH/lib
pythonCMD="python -u -W ignore"

$pythonCMD python_worker_template.py 
